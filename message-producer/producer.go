/*
 * Copyright (C) Peak Analytica - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Guillermo Vazquez and Damaso Valdez --<hello@peakanalytica.com>
 */

package message_producer

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type MessageProducer struct {
	Broker string
	Topic  string
}

func (mp MessageProducer) SendMessage(message string, headers map[string]string) {
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": mp.Broker})

	if err != nil {
		fmt.Printf("Failed to create producer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Producer %v\n", p)

	// Optional delivery channel, if not specified the Producer object's
	// .Events channel is used.
	deliveryChan := make(chan kafka.Event)

	// Convert headers
	kafkaHeaders := mapToHeaders(headers)

	err = p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &mp.Topic, Partition: kafka.PartitionAny},
		Value:          []byte(message),
		Headers:        kafkaHeaders,
	}, deliveryChan)

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if m.TopicPartition.Error != nil {
		fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
	} else {
		fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
			*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
	}

	close(deliveryChan)
}

func mapToHeaders(headers map[string]string) []kafka.Header {
	var kafkaHeaders []kafka.Header
	for key, value := range headers {
		kafkaHeader := kafka.Header{Key: key, Value: []byte(value)}
		kafkaHeaders = append(kafkaHeaders, kafkaHeader)
	}
	return kafkaHeaders
}